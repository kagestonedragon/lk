<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BidFactory extends Factory
{
    /**
     * @return string[]
     */
    public function definition(): array
    {
        return [
            'description' => 'Описание',
        ];
    }
}
