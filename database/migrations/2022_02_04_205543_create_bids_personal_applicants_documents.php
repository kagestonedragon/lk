<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBidsPersonalApplicantsDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bids_personal_applicants_documents', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('applicant_id')->unique();
            $table->string('series');
            $table->string('number');
            $table->string('date');
            $table->string('issued_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bids_personal_applicants_documents');
    }
}
