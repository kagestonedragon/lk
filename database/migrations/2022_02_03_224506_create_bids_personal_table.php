<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBidsPersonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bids_personal', function (Blueprint $table) {
            $table->id();
            $table->integer('status_id');
            $table->bigInteger('applicant_id');
            $table->string('type');
            $table->string('location');
            $table->string('conditional_number');
            $table->string('voltage_level');
            $table->string('voltage_level_max');
            $table->string('devices');
            $table->string('devices_reliability');
            $table->string('supplier');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bids_personal');
    }
}
