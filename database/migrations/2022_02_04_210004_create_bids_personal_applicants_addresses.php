<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBidsPersonalApplicantsAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bids_personal_applicants_addresses', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('applicant_id')->unique();
            $table->string('region');
            $table->string('city');
            $table->string('street');
            $table->string('building');
            $table->string('post_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bids_personal_applicants_addresses');
    }
}
