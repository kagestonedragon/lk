<?php

use App\Models\Bids\Status;
use Illuminate\Database\Migrations\Migration;

class CreateBidsEntryStatusElement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $status = new Status([
            'code' => 'N',
            'name' => 'Принято',
        ]);

        if ($status->save() === false) {
            throw new \RuntimeException('Не удалось добавить статус');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Status::query()
            ->where('code', '=', 'N')
            ->delete();
    }
}
