<?php

namespace App\Repositories\Models\Users;

use App\Models\User;
use App\Repositories\Interfaces\Models\Users\UsersRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UsersRepository implements UsersRepositoryInterface
{
    /**
     * @param string $email
     * @return User
     */
    public function getByEmail(string $email): User
    {
        /** @var User $user */
        $user = User::query()
            ->where('email', '=', $email)
            ->first();

        if ($user === null) {
            throw new ModelNotFoundException();
        }

        return $user;
    }
}
