<?php

namespace App\Repositories\Models\Bids\Personal;

use App\Models\Bids\AbstractBid;
use App\Models\User;
use App\Repositories\Models\Bids\AbstractBidsRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BidsRepository extends AbstractBidsRepository
{
    /**
     * @param int $id
     * @param User|null $user
     * @return AbstractBid
     */
    public function getById(int $id, ?User $user = null): AbstractBid
    {
        $query = $this->modelClass::query()
            ->with([
                'applicant' => function($query) {
                    $query
                        ->with('document')
                        ->with('address');
                },
            ])
            ->with('status')
            ->where('id', '=', $id);

        if ($user instanceof User) {
            $query
                ->where('created_by', '=', $user->id);
        }

        /** @var AbstractBid $bid */
        $bid = $query->first();

        if ($bid === null) {
            throw new ModelNotFoundException();
        }

        return $bid;
    }
}
