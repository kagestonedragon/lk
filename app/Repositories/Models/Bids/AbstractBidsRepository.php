<?php

namespace App\Repositories\Models\Bids;

use App\Models\Bids\AbstractBid;
use App\Models\User;
use App\Repositories\Interfaces\Models\Bids\BidsRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

abstract class AbstractBidsRepository implements BidsRepositoryInterface
{
    /** @var string|AbstractBid $modelClass */
    protected $modelClass;

    /**
     * BidsRepository constructor.
     * @param string $modelClass
     */
    public function __construct(string $modelClass)
    {
        $this->modelClass = $modelClass;
    }

    /**
     * @param User $user
     * @return Collection
     */
    public function getAllByUser(User $user): Collection
    {
        return $this->modelClass::query()
            ->select([
                'id',
                'created_at',
                'status_id'
            ])
            ->with([
                'status' => function($query) {
                    $query->select('id', 'name');
                }
            ])
            ->where('created_by', '=', $user->id)
            ->orderBy('id', 'desc')
            ->get();
    }
}
