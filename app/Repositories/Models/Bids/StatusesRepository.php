<?php

namespace App\Repositories\Models\Bids;

use App\Models\Bids\Status;
use App\Repositories\Interfaces\Models\Bids\StatusesRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StatusesRepository implements StatusesRepositoryInterface
{
    /**
     * @param string $code
     * @return Status
     */
    public function getByCode(string $code): Status
    {
        /** @var Status $status */
        $status = Status::query()
            ->where('code', '=', $code)
            ->first();

        if ($status === null) {
            throw new ModelNotFoundException();
        }

        return $status;
    }

    /**
     * @return Status
     */
    public function getInitialStatus(): Status
    {
        return $this->getByCode(config('bids.statuses.initial'));
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return Status::all();
    }
}
