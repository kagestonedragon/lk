<?php

namespace App\Repositories\Interfaces\Models\Users;

use App\Models\User;

interface UsersRepositoryInterface
{
    /**
     * @param string $email
     * @return User
     */
    public function getByEmail(string $email): User;
}
