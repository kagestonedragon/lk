<?php

namespace App\Repositories\Interfaces\Models\Bids;

use App\Models\Bids\AbstractBid;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface BidsRepositoryInterface
{
    /**
     * @param User $user
     * @return Collection
     */
    public function getAllByUser(User $user): Collection;

    /**
     * @param int $id
     * @param User|null $user
     * @return AbstractBid
     */
    public function getById(int $id, ?User $user = null): AbstractBid;
}
