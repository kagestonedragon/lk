<?php

namespace App\Repositories\Interfaces\Models\Bids;

use App\Models\Bids\Status;
use Illuminate\Database\Eloquent\Collection;

interface StatusesRepositoryInterface
{
    /**
     * @return Status
     */
    public function getInitialStatus(): Status;

    /**
     * @param string $code
     * @return Status
     */
    public function getByCode(string $code): Status;

    /**
     * @return Collection
     */
    public function getAll(): Collection;
}
