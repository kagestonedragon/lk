<?php

namespace App\Support\Media;

use App\Models\User;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\PathGenerator as PathGeneratorContract;

class PathGenerator implements PathGeneratorContract
{
    /**
     * @param Media $media
     * @return string
     */
    public function getPath(Media $media): string
    {
        /** @var User $user */
        $user = request()->user();

        if ($user === null) {
            return '';
        }

        return $user->id . '/';
    }

    /**
     * @param Media $media
     * @return string
     */
    public function getPathForConversions(Media $media): string
    {
        return $this->getPath($media) . '/conversions';
    }

    /**
     * @param Media $media
     * @return string
     */
    public function getPathForResponsiveImages(Media $media): string
    {
        return $this->getPath($media) . '/responsive';
    }
}
