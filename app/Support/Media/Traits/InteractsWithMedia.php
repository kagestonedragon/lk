<?php

namespace App\Support\Media\Traits;

use Illuminate\Support\Str;
use Spatie\MediaLibrary\InteractsWithMedia as BaseInteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\FileAdder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait InteractsWithMedia
{
    use BaseInteractsWithMedia;

    /**
     * @param UploadedFile $file
     * @return FileAdder
     */
    public function addHashedMedia(UploadedFile $file): FileAdder
    {
        return $this
            ->addMedia($file)
            ->usingFileName(
                $this->generateUniqueName($file)
            );
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    protected function generateUniqueName(UploadedFile $file): string
    {
        return sprintf(
            "%s.%s",
            md5(Str::uuid()),
            $file->getClientOriginalExtension()
        );
    }
}
