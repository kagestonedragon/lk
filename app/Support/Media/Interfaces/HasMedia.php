<?php

namespace App\Support\Media\Interfaces;

use Spatie\MediaLibrary\HasMedia as BaseHasMedia;
use Spatie\MediaLibrary\MediaCollections\FileAdder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface HasMedia extends BaseHasMedia
{
    /**
     * @param UploadedFile $file
     * @return FileAdder
     */
    public function addHashedMedia(UploadedFile $file): FileAdder;
}
