<?php

namespace App\Exceptions\Factories;

use Throwable;

class SaveException extends \RuntimeException
{
    /** @var object $object */
    protected object $object;

    public function __construct(object $object, $message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
