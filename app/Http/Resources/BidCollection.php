<?php

namespace App\Http\Resources;

use App\Models\Bids\AbstractBid;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BidCollection extends ResourceCollection
{
    /**
     * @param mixed $request
     * @return array
     */
    public function toArray(mixed $request): array
    {
        $items = [];

        /** @var AbstractBid $item */
        foreach ($this->collection as $item) {
            $items[] = [
                'id' => $item->id,
                'created_at' => $item->createdAt,
                'status' => $item->status->name,
            ];
        }

        return $items;
    }
}
