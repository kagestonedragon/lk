<?php

namespace App\Http\Resources;

use App\Models\Bids\AbstractBid;
use App\Models\Bids\Personal\Bid;
use App\Models\Media;
use Illuminate\Http\Resources\Json\JsonResource;

class BidResource extends JsonResource
{
    /**
     * @param mixed $request
     * @return array
     */
    public function toArray(mixed $request): array
    {
        /** @var Bid $bid */
        $bid = $this->resource;

        return [
            'id' => $bid->id,
            'status' => [
                'id' => $bid->status->id,
                'name' => $bid->status->name,
            ],
            'applicant' => [
                'first_name' => $bid->applicant->firstName,
                'last_name' => $bid->applicant->lastName,
                'second_name' => $bid->applicant->secondName,
                'inn' => $bid->applicant->inn,
                'document' => [
                    'series' => $bid->applicant->document->series,
                    'number' => $bid->applicant->document->number,
                    'date' => $bid->applicant->document->date,
                    'issued_by' => $bid->applicant->document->issuedBy,
                ],
                'address' => [
                    'region' => $bid->applicant->address->region,
                    'city' => $bid->applicant->address->city,
                    'street' => $bid->applicant->address->street,
                    'building' => $bid->applicant->address->building,
                    'post_code' => $bid->applicant->address->postCode,
                 ],
            ],
            'description' => [
                'type' => $bid->type,
                'location' => $bid->location,
                'conditional_number' => $bid->conditionalNumber,
                'voltage_level' => $bid->voltageLevel,
                'voltage_level_max' => $bid->voltageLevelMax,
                'devices' => $bid->devices,
                'devices_reliability' => $bid->devicesReliability,
                'supplier' => $bid->supplier,
            ],
            'documents' => $this->getDocuments($bid),
        ];
    }

    protected function getDocuments(AbstractBid $bid): array
    {
        $result = [];

        /** @var Media $item */
        foreach ($bid->media()->get() as $item) {
            $result[$item->collection_name][] = [
                'name' => $item->name,
                'url' => $item->file_name,
            ];
        }

        return $result;
    }
}
