<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

abstract class AbstractFormRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        $ability = static::getAbilityName();

        return $ability === null || $this->user()->tokenCan($ability);
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public function get(string $key, $default = null): mixed
    {
        $result = parent::get($key, $default);

        if ($result === null) {
            $result = $this->input($key, $default);
        }

        return $result;
    }

    /**
     * @param null $guard
     * @return User|null
     */
    public function user($guard = null): ?User
    {
        return parent::user($guard);
    }

    /**
     * @return string|null
     */
    public static function getAbilityName(): ?string
    {
        return null;
    }
}
