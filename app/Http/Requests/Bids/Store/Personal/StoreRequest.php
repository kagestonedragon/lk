<?php

namespace App\Http\Requests\Bids\Store\Personal;

use App\Http\Requests\Bids\Store\AbstractStoreRequest;

class StoreRequest extends AbstractStoreRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'layout_documents' => [
                'required',
            ],
            'ownership_documents' => [
                'required',
            ],
            'type' => [
                'required',
            ],
            'location' => [
                'required',
            ],
            'conditional_number' => [
                'required',
            ],
            'voltage_level' => [
                'required',
            ],
            'voltage_level_max' => [
                'required'
            ],
            'devices' => [
                'required',
            ],
            'devices_reliability' => [
                'required',
            ],
            'supplier' => [
                'required',
            ],
            'applicant.first_name' => [
                'required',
            ],
            'applicant.last_name' => [
                'required',
            ],
            'applicant.second_name' => [
                'required',
            ],
            'applicant.inn' => [
                'required',
            ],
            'applicant.contacts.phone' => [
                'required',
            ],
            'applicant.contacts.email' => [
                'required',
                'email',
            ],
            'applicant.document.series' => [
                'required',
            ],
            'applicant.document.number' => [
                'required',
            ],
            'applicant.document.date' => [
                'required',
            ],
            'applicant.document.issued_by' => [
                'required',
            ],
            'applicant.address.region' => [
                'required',
            ],
            'applicant.address.city' => [
                'required',
            ],
            'applicant.address.street' => [
                'required',
            ],
            'applicant.address.building' => [
                'required',
            ],
            'applicant.address.post_code' => [
                'required',
            ],
        ];
    }

    /**
     * @return string
     */
    public static function getAbilityName(): string
    {
        return 'bid-personal:store';
    }
}
