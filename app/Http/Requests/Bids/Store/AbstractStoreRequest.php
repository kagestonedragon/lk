<?php

namespace App\Http\Requests\Bids\Store;

use App\Http\Requests\AbstractMultipartJsonRequest;

abstract class AbstractStoreRequest extends AbstractMultipartJsonRequest
{

}
