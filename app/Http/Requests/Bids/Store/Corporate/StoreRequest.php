<?php

namespace App\Http\Requests\Bids\Store\Corporate;

use App\Http\Requests\Bids\Store\AbstractStoreRequest;

class StoreRequest extends AbstractStoreRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [

        ];
    }

    /**
     * @return string
     */
    public static function getAbilityName(): string
    {
        return 'bid-corporate:store';
    }
}
