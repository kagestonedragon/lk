<?php

namespace App\Http\Requests;

use Symfony\Component\HttpKernel\Exception\HttpException;

abstract class AbstractMultipartJsonRequest extends AbstractFormRequest
{
    /**
     * @return void
     */
    protected function prepareForValidation(): void
    {
        $result = json_decode($this->request->get('data'), true);

        if ($result === null) {
            throw new HttpException(422);
        }

        $this->request->add($result);
    }
}
