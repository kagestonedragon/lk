<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\AbstractFormRequest;

class AuthRequest extends AbstractFormRequest
{
    /**
     * @return \string[][]
     */
    public function rules(): array
    {
        return [
            'email' => [
                'required',
            ],
            'password' => [
                'required',
            ],
        ];
    }
}
