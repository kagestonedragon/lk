<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\AbstractFormRequest;

class StoreRequest extends AbstractFormRequest
{
    /**
     * @return \string[][]
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'unique:users,name',
                'min:6'
            ],
            'email' => [
                'required',
                'unique:users,email',
                'email',
            ],
            'password' => [
                'required',
                'min:6',
            ],
        ];
    }
}
