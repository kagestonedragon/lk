<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\Models\Bids\StatusesRepositoryInterface;
use Illuminate\Http\JsonResponse;

class BidsStatusesController extends AbstractController
{
    /** @var StatusesRepositoryInterface $bidsStatusesRepository */
    protected StatusesRepositoryInterface $bidsStatusesRepository;

    /**
     * BidsStatusesController constructor.
     * @param StatusesRepositoryInterface $bidsStatusesRepository
     */
    public function __construct(
        StatusesRepositoryInterface $bidsStatusesRepository
    ) {
        $this->bidsStatusesRepository = $bidsStatusesRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return new JsonResponse(
            $this->bidsStatusesRepository->getAll()
        );
    }
}
