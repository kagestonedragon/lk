<?php

namespace App\Http\Controllers;

use App\Factories\Interfaces\Models\Users\UsersFactoryInterface;
use App\Http\Requests\Users\AuthRequest;
use App\Http\Requests\Users\StoreRequest;
use Illuminate\Http\JsonResponse;

class UsersController extends AbstractController
{
    /** @var UsersFactoryInterface $usersFactory */
    protected UsersFactoryInterface $usersFactory;

    /**
     * UsersController constructor.
     * @param UsersFactoryInterface $usersFactory
     */
    public function __construct(
        UsersFactoryInterface $usersFactory
    ) {
        $this->usersFactory = $usersFactory;
    }

    /**
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        $user = $this->usersFactory->createByRequest($request);

        return new JsonResponse([
            'id' => $user->id,
        ]);
    }

    /**
     * @param AuthRequest $request
     * @return JsonResponse
     */
    public function authenticate(AuthRequest $request): JsonResponse
    {
        $token = $this->usersFactory->createAccessTokenByRequest($request);

        return new JsonResponse([
            'token' => $token->plainTextToken,
        ]);
    }
}
