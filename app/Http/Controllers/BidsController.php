<?php

namespace App\Http\Controllers;

use App\Factories\Interfaces\Models\Bids\BidsFactoryInterface;
use App\Http\Requests\Bids\Store\AbstractStoreRequest;
use App\Http\Resources\BidCollection;
use App\Repositories\Interfaces\Models\Bids\BidsRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BidsController extends AbstractController
{
    /** @var BidsFactoryInterface $bidsFactory */
    protected BidsFactoryInterface $bidsFactory;

    /** @var BidsRepositoryInterface $bidsRepository */
    protected BidsRepositoryInterface $bidsRepository;

    /**
     * BidsController constructor.
     * @param BidsFactoryInterface $bidsFactory
     * @param BidsRepositoryInterface $bidsRepository
     */
    public function __construct(
        BidsFactoryInterface $bidsFactory,
        BidsRepositoryInterface $bidsRepository
    ) {
        $this->bidsFactory = $bidsFactory;
        $this->bidsRepository = $bidsRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $collection = $this->bidsRepository->getAllByUser($request->user());

        return new JsonResponse(
            new BidCollection($collection)
        );
    }

    /**
     * @param AbstractStoreRequest $request
     * @return JsonResponse
     */
    public function store(AbstractStoreRequest $request): JsonResponse
    {
        $bid = $this->bidsFactory->createByRequest($request);

        return new JsonResponse([
            'id' => $bid->id,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function show(Request $request, int $id): JsonResponse
    {
        $bid = $this->bidsRepository->getById($id, $request->user());

        return new JsonResponse(
            $bid->getResource()
        );
    }
}
