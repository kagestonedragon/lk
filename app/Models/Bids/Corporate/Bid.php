<?php

namespace App\Models\Bids\Corporate;

use App\Models\Bids\AbstractBid;

/**
 * @property        Applicant      $applicant
 */
class Bid extends AbstractBid
{
    /** @var string $table */
    protected $table = 'bids_corporate';

    /**
     * @return string
     */
    protected function getApplicantModelClass(): string
    {
        return Applicant::class;
    }
}
