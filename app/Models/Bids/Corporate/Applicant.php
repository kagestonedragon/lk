<?php

namespace App\Models\Bids\Corporate;

use App\Models\Bids\AbstractApplicant;

class Applicant extends AbstractApplicant
{
    /** @var string $table */
    protected $table = 'bids_corporate_applicants';
}
