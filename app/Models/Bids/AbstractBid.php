<?php

namespace App\Models\Bids;

use App\Models\AbstractModel;
use App\Models\User;
use App\Support\Media\Interfaces\HasMedia;
use App\Support\Media\Traits\InteractsWithMedia;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

/**
 * @property    integer                 $id
 * @property    AbstractApplicant       $applicant
 * @property    Status                  $status
 * @property    integer                 $applicantId
 * @property    integer                 $statusId
 * @property    integer                 $createdBy
 * @property    integer                 $updatedBy
 * @property    Carbon                  $createdAt
 */
abstract class AbstractBid extends AbstractModel implements HasMedia
{
    use InteractsWithMedia;

    /**
     * @return BelongsTo
     */
    public function applicant(): BelongsTo
    {
        return $this->belongsTo($this->getApplicantClass());
    }

    /**
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * @return string
     */
    abstract protected function getApplicantClass(): string;

    /**
     * @return void
     */
    public static function boot(): void
    {
        parent::boot();

        /** @var User $user */
        $user = request()->user();

        static::creating(function(AbstractBid $model) use ($user) {
            $model->createdBy = $user->id;
            $model->updatedBy = $user->id;
        });

        static::updating(function(AbstractBid $model) use ($user) {
            $model->updatedBy = $user->id;
        });
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function getCreatedAtAttribute(mixed $value): string
    {
        return Carbon::parse($value)->format('Y-m-d H:i');
    }

    /**
     * @return JsonResource
     */
    abstract function getResource(): JsonResource;
}
