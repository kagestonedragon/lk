<?php

namespace App\Models\Bids\Personal\Applicant;

use App\Models\AbstractModel;

/**
 * @property        integer     $id
 * @property        integer     $applicantId
 * @property        string      $region
 * @property        string      $city
 * @property        string      $street
 * @property        string      $building
 * @property        string      $postCode
 */
class Address extends AbstractModel
{
    /** @var string $table */
    protected $table = 'bids_personal_applicants_addresses';

    /** @var bool $timestamps */
    public $timestamps = false;

    /** @var string[] $fillable */
    protected $fillable = [

    ];
}
