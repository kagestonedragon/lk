<?php

namespace App\Models\Bids\Personal\Applicant;

use App\Models\Bids\AbstractApplicant;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property        string      $firstName
 * @property        string      $lastName
 * @property        string      $secondName
 * @property        string      $phone
 * @property        string      $email
 * @property        string      $inn
 * @property        Document    $document
 * @property        Address     $address
 */
class Applicant extends AbstractApplicant
{
    /** @var string $table */
    protected $table = 'bids_personal_applicants';

    /** @var bool $timestamps */
    public $timestamps = false;

    /** @var string[] $fillable */
    protected $fillable = [
        'first_name',
        'last_name',
        'second_name',
        'phone',
        'email',
        'inn',
    ];

    /**
     * @return HasOne
     */
    public function document(): HasOne
    {
        return $this->hasOne(Document::class);
    }

    public function address(): HasOne
    {
        return $this->hasOne(Address::class);
    }
}
