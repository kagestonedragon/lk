<?php

namespace App\Models\Bids\Personal\Applicant;

use App\Models\AbstractModel;

/**
 * @property        integer     $id
 * @property        integer     $applicantId
 * @property        string      $series
 * @property        string      $number
 * @property        string      $date
 * @property        string      $issuedBy
 */
class Document extends AbstractModel
{
    /** @var string $table */
    protected $table = 'bids_personal_applicants_documents';

    /** @var bool $timestamps */
    public $timestamps = false;

    /** @var string[] $fillable */
    protected $fillable = [
        'series',
        'number',
        'date',
        'issued_by'
    ];
}
