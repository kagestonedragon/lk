<?php

namespace App\Models\Bids\Personal;

use App\Http\Resources\BidResource;
use App\Models\Bids\AbstractBid;
use App\Models\Bids\Personal\Applicant\Applicant;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property        Applicant   $applicant
 * @property        string      $type
 * @property        string      $location
 * @property        string      $conditionalNumber
 * @property        string      $voltageLevel
 * @property        string      $voltageLevelMax
 * @property        string      $devices
 * @property        string      $devicesReliability
 * @property        string      $supplier
 */
class Bid extends AbstractBid
{
    /** @var string $table */
    protected $table = 'bids_personal';

    /** @var string[] $fillable */
    protected $fillable = [
        'type',
        'location',
        'conditional_number',
        'voltage_level',
        'voltage_level_max',
        'devices',
        'devices_reliability',
        'supplier'
    ];

    /**
     * @return string
     */
    protected function getApplicantClass(): string
    {
        return Applicant::class;
    }

    public function getResource(): JsonResource
    {
        return new BidResource($this);
    }
}
