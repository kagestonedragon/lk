<?php

namespace App\Models\Bids;

use App\Models\AbstractModel;

/**
 * @property        integer     $id
 * @property        string      $code
 * @property        string      $name
 */
class Status extends AbstractModel
{
    /** @var string $table */
    protected $table = 'bids_statuses';

    /** @var bool $timestamps */
    public $timestamps = false;

    /** @var string[] $fillable */
    protected $fillable = [
        'code',
        'name',
    ];
}
