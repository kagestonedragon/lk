<?php

namespace App\Providers\Factories\Users;

use App\Factories\Interfaces\Models\Users\UsersFactoryInterface;
use App\Factories\Interfaces\Support\Utils\AccessTokensFactoryInterface;
use App\Factories\Models\Users\UsersFactory;
use App\Repositories\Interfaces\Models\Users\UsersRepositoryInterface;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class UsersFactoryProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(UsersFactoryInterface::class, function(Application $application) {
            return new UsersFactory(
                $application->get(AccessTokensFactoryInterface::class),
                $application->get(UsersRepositoryInterface::class)
            );
        });
    }
}
