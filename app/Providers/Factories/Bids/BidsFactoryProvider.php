<?php

namespace App\Providers\Factories\Bids;

use App\Factories\Interfaces\Models\Bids\ApplicantsFactoryInterface;
use App\Factories\Interfaces\Models\Bids\BidsFactoryInterface;
use App\Factories\Interfaces\Models\Bids\DocumentsFactoryInterface;
use App\Factories\Models\Bids\Personal\BidsFactory;
use App\Repositories\Interfaces\Models\Bids\StatusesRepositoryInterface;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class BidsFactoryProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(BidsFactoryInterface::class, function(Application $application) {
            return new BidsFactory(
                $application->get(ApplicantsFactoryInterface::class),
                $application->get(DocumentsFactoryInterface::class),
                $application->get(StatusesRepositoryInterface::class)
            );
        });
    }
}
