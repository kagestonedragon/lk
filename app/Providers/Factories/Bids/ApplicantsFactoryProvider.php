<?php

namespace App\Providers\Factories\Bids;

use App\Factories\Interfaces\Models\Bids\ApplicantsFactoryInterface;
use App\Factories\Models\Bids\Personal\ApplicantsFactory;
use Illuminate\Support\ServiceProvider;

class ApplicantsFactoryProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(ApplicantsFactoryInterface::class, function() {
            return new ApplicantsFactory();
        });
    }
}
