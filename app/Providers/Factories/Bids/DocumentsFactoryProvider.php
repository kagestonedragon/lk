<?php

namespace App\Providers\Factories\Bids;

use App\Factories\Interfaces\Models\Bids\DocumentsFactoryInterface;
use App\Factories\Models\Bids\Personal\DocumentsFactory;
use Illuminate\Support\ServiceProvider;

class DocumentsFactoryProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(DocumentsFactoryInterface::class, function() {
            return new DocumentsFactory();
        });
    }
}
