<?php

namespace App\Providers\Factories\Utils;

use App\Factories\Interfaces\Support\Utils\AccessTokensFactoryInterface;
use App\Factories\Support\Utils\AccessTokensFactory;
use Illuminate\Support\ServiceProvider;

class AccessTokenFactoryProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(AccessTokensFactoryInterface::class, function() {
            return new AccessTokensFactory();
        });
    }
}
