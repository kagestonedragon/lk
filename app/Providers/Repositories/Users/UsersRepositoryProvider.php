<?php

namespace App\Providers\Repositories\Users;

use App\Repositories\Interfaces\Models\Users\UsersRepositoryInterface;
use App\Repositories\Models\Users\UsersRepository;
use Illuminate\Support\ServiceProvider;

class UsersRepositoryProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(UsersRepositoryInterface::class, function() {
            return new UsersRepository();
        });
    }
}
