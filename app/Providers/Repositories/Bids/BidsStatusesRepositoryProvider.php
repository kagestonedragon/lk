<?php

namespace App\Providers\Repositories\Bids;

use App\Repositories\Interfaces\Models\Bids\StatusesRepositoryInterface;
use App\Repositories\Models\Bids\StatusesRepository;
use Illuminate\Support\ServiceProvider;

class BidsStatusesRepositoryProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(StatusesRepositoryInterface::class, function() {
            return new StatusesRepository();
        });
    }
}
