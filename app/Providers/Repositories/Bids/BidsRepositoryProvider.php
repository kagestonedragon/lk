<?php

namespace App\Providers\Repositories\Bids;

use App\Models\Bids\Personal\Bid as PersonalBid;
use App\Repositories\Interfaces\Models\Bids\BidsRepositoryInterface;
use App\Repositories\Models\Bids\Personal\BidsRepository;
use Illuminate\Support\ServiceProvider;

class BidsRepositoryProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(BidsRepositoryInterface::class, function() {
//            $model = request()->route()->name('corporate')
//                ? CorporateBid::class
//                : PersonalBid::class;

            return new BidsRepository(PersonalBid::class);
        });
    }
}
