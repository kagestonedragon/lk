<?php

namespace App\Providers\Requests\Bids;

use App\Http\Requests\Bids\Store\AbstractStoreRequest;
use App\Http\Requests\Bids\Store\Corporate\StoreRequest as CorporateStoreRequest;
use App\Http\Requests\Bids\Store\Personal\StoreRequest as PersonalStoreRequest;
use Illuminate\Support\ServiceProvider;

class StoreRequestProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(AbstractStoreRequest::class, function() {
            return request()->route()->getName() === 'corporate'
                ? new CorporateStoreRequest()
                : new PersonalStoreRequest();
        });
    }
}
