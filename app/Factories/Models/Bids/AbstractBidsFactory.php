<?php

namespace App\Factories\Models\Bids;

use App\Factories\Interfaces\Models\Bids\ApplicantsFactoryInterface;
use App\Factories\Interfaces\Models\Bids\BidsFactoryInterface;
use App\Factories\Interfaces\Models\Bids\DocumentsFactoryInterface;
use App\Factories\Support\Models\Traits\Saveable;
use App\Http\Requests\Bids\Store\AbstractStoreRequest;
use App\Models\Bids\AbstractApplicant;
use App\Models\Bids\AbstractBid;
use App\Repositories\Interfaces\Models\Bids\StatusesRepositoryInterface;
use Illuminate\Support\Facades\DB;

abstract class AbstractBidsFactory implements BidsFactoryInterface
{
    use Saveable;

    /** @var ApplicantsFactoryInterface $applicantFactory */
    protected ApplicantsFactoryInterface $applicantFactory;

    /** @var DocumentsFactoryInterface $documentsFactory */
    protected DocumentsFactoryInterface $documentsFactory;

    /** @var StatusesRepositoryInterface $statusesRepository */
    protected StatusesRepositoryInterface $statusesRepository;

    /**
     * AbstractBidsFactory constructor.
     * @param ApplicantsFactoryInterface $applicantFactory
     * @param DocumentsFactoryInterface $documentsFactory
     * @param StatusesRepositoryInterface $statusesRepository
     */
    public function __construct(
        ApplicantsFactoryInterface $applicantFactory,
        DocumentsFactoryInterface $documentsFactory,
        StatusesRepositoryInterface $statusesRepository
    ) {
        $this->applicantFactory = $applicantFactory;
        $this->documentsFactory = $documentsFactory;
        $this->statusesRepository = $statusesRepository;
    }

    /**
     * @param AbstractStoreRequest $request
     * @return AbstractBid
     */
    public function createByRequest(AbstractStoreRequest $request): AbstractBid
    {
        return DB::transaction(function() use ($request) {
            $applicant = $this->applicantFactory->createByRequest($request);

            $bid = $this->create($applicant, $request);
            $this->save($bid);

            return $this->documentsFactory->createByRequest($bid, $request);
        });
    }

    /**
     * @param AbstractApplicant $applicant
     * @param AbstractStoreRequest $request
     * @return AbstractBid
     */
    abstract protected function create(AbstractApplicant $applicant, AbstractStoreRequest $request): AbstractBid;
}
