<?php

namespace App\Factories\Models\Bids\Personal;

use App\Factories\Interfaces\Models\Bids\ApplicantsFactoryInterface;

use App\Factories\Support\Models\Traits\Saveable;
use App\Http\Requests\Bids\Store\AbstractStoreRequest;
use App\Models\Bids\AbstractApplicant;
use App\Models\Bids\Personal\Applicant\Address;
use App\Models\Bids\Personal\Applicant\Applicant;
use App\Models\Bids\Personal\Applicant\Document;

class ApplicantsFactory implements ApplicantsFactoryInterface
{
    use Saveable;

    /**
     * @param AbstractStoreRequest $request
     * @return AbstractApplicant
     */
    public function createByRequest(AbstractStoreRequest $request): AbstractApplicant
    {
        $applicant = $this->create($request);
        $this->save($applicant);

        $document = $this->createDocument($applicant, $request);
        $this->save($document);

        $address = $this->createAddress($applicant, $request);
        $this->save($address);

        return $applicant;
    }

    /**
     * @param AbstractStoreRequest $request
     * @return Applicant
     */
    protected function create(AbstractStoreRequest $request): Applicant
    {
        $applicant = new Applicant();

        $applicant->firstName = $request->get('applicant.first_name');
        $applicant->lastName = $request->get('applicant.last_name');
        $applicant->secondName = $request->get('applicant.second_name');
        $applicant->inn = $request->get('applicant.inn');
        $applicant->phone = $request->get('applicant.contacts.phone');
        $applicant->email = $request->get('applicant.contacts.email');

        return $applicant;
    }

    /**
     * @param Applicant $applicant
     * @param AbstractStoreRequest $request
     * @return Document
     */
    protected function createDocument(Applicant $applicant, AbstractStoreRequest $request): Document
    {
        $document = new Document();

        $document->applicantId = $applicant->id;
        $document->series = $request->get('applicant.document.series');
        $document->number = $request->get('applicant.document.number');
        $document->date = $request->get('applicant.document.date');
        $document->issuedBy = $request->get('applicant.document.issued_by');

        return $document;
    }

    /**
     * @param Applicant $applicant
     * @param AbstractStoreRequest $request
     * @return Address
     */
    protected function createAddress(Applicant $applicant, AbstractStoreRequest $request): Address
    {
        $address = new Address();

        $address->applicantId = $applicant->id;
        $address->region = $request->get('applicant.address.region');
        $address->city = $request->get('applicant.address.city');
        $address->street = $request->get('applicant.address.street');
        $address->building = $request->get('applicant.address.building');
        $address->postCode = $request->get('applicant.address.post_code');

        return $address;
    }
}
