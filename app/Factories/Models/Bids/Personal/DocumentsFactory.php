<?php

namespace App\Factories\Models\Bids\Personal;

use App\Factories\Interfaces\Models\Bids\DocumentsFactoryInterface;
use App\Factories\Support\Models\Traits\Mediable;
use App\Http\Requests\Bids\Store\AbstractStoreRequest;
use App\Models\Bids\AbstractBid;

class DocumentsFactory implements DocumentsFactoryInterface
{
    use Mediable;

    /**
     * @param AbstractBid $bid
     * @param AbstractStoreRequest $request
     * @return AbstractBid
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     */
    public function createByRequest(AbstractBid $bid, AbstractStoreRequest $request): AbstractBid
    {
        $this->addMedia(
            $bid,
            $request->file('layout_documents'),
            'layout'
        );

        $this->addMedia(
            $bid,
            $request->file('ownership_documents'),
            'ownership'
        );

        return $bid;
    }
}
