<?php

namespace App\Factories\Models\Bids\Personal;

use App\Factories\Models\Bids\AbstractBidsFactory;
use App\Http\Requests\Bids\Store\AbstractStoreRequest;
use App\Models\Bids\AbstractApplicant;
use App\Models\Bids\AbstractBid;
use App\Models\Bids\Personal\Bid;

class BidsFactory extends AbstractBidsFactory
{
    /**
     * @param AbstractApplicant $applicant
     * @param AbstractStoreRequest $request
     * @return AbstractBid
     */
    protected function create(AbstractApplicant $applicant, AbstractStoreRequest $request): AbstractBid
    {
        $bid = new Bid();

        $bid->applicantId = $applicant->id;
        $bid->statusId = $this->statusesRepository->getInitialStatus()->id;
        $bid->type = $request->get('type');
        $bid->location = $request->get('location');
        $bid->conditionalNumber = $request->get('conditional_number');
        $bid->voltageLevel = $request->get('voltage_level');
        $bid->voltageLevelMax = $request->get('voltage_level_max');
        $bid->devices = $request->get('devices');
        $bid->devicesReliability = $request->get('devices_reliability');
        $bid->supplier = $request->get('supplier');

        return $bid;
    }
}
