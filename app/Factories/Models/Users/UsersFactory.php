<?php

namespace App\Factories\Models\Users;

use App\Factories\Interfaces\Models\Users\UsersFactoryInterface;
use App\Factories\Interfaces\Support\Utils\AccessTokensFactoryInterface;
use App\Factories\Support\Models\Traits\Saveable;
use App\Http\Requests\Users\AuthRequest;
use App\Http\Requests\Users\StoreRequest;
use App\Models\User;
use App\Repositories\Interfaces\Models\Users\UsersRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\NewAccessToken;

class UsersFactory implements UsersFactoryInterface
{
    use Saveable;

    /** @var AccessTokensFactoryInterface $accessTokensFactory */
    protected AccessTokensFactoryInterface $accessTokensFactory;

    /** @var UsersRepositoryInterface $usersRepository */
    protected UsersRepositoryInterface $usersRepository;

    /**
     * UsersFactory constructor.
     * @param AccessTokensFactoryInterface $accessTokensFactory
     * @param UsersRepositoryInterface $usersRepository
     */
    public function __construct(
        AccessTokensFactoryInterface $accessTokensFactory,
        UsersRepositoryInterface $usersRepository
    ) {
        $this->accessTokensFactory = $accessTokensFactory;
        $this->usersRepository = $usersRepository;
    }

    /**
     * @param StoreRequest $request
     * @return User
     */
    public function createByRequest(StoreRequest $request): User
    {
        $user = $this->create($request);

        $this->save($user);

        return $user;
    }

    /**
     * @param AuthRequest $request
     * @return NewAccessToken
     */
    public function createAccessTokenByRequest(AuthRequest $request): NewAccessToken
    {
        $user = $this->usersRepository->getByEmail($request->get('email'));

        if (Hash::check($request->get('password'), $user->password) === false) {
            throw new ModelNotFoundException();
        }

        return $this->accessTokensFactory->createByUser($user);
    }

    /**
     * @param StoreRequest $request
     * @return User
     */
    protected function create(StoreRequest $request): User
    {
        $user = new User();

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));

        return $user;
    }
}
