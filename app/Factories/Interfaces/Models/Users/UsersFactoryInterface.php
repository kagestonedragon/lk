<?php

namespace App\Factories\Interfaces\Models\Users;

use App\Http\Requests\Users\AuthRequest;
use App\Http\Requests\Users\StoreRequest;
use App\Models\User;
use Laravel\Sanctum\NewAccessToken;

interface UsersFactoryInterface
{
    /**
     * @param StoreRequest $request
     * @return User
     */
    public function createByRequest(StoreRequest $request): User;

    /**
     * @param AuthRequest $request
     * @return NewAccessToken
     */
    public function createAccessTokenByRequest(AuthRequest $request): NewAccessToken;
}
