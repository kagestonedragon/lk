<?php

namespace App\Factories\Interfaces\Models\Bids;

use App\Http\Requests\Bids\Store\AbstractStoreRequest;
use App\Models\Bids\AbstractBid;

interface BidsFactoryInterface
{
    /**
     * @param AbstractStoreRequest $request
     * @return AbstractBid
     */
    public function createByRequest(AbstractStoreRequest $request): AbstractBid;
}
