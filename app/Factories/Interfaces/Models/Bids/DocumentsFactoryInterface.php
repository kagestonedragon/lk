<?php

namespace App\Factories\Interfaces\Models\Bids;

use App\Http\Requests\Bids\Store\AbstractStoreRequest;
use App\Models\Bids\AbstractBid;

interface DocumentsFactoryInterface
{
    /**
     * @param AbstractBid $bid
     * @param AbstractStoreRequest $request
     * @return AbstractBid
     */
    public function createByRequest(AbstractBid $bid, AbstractStoreRequest $request): AbstractBid;
}
