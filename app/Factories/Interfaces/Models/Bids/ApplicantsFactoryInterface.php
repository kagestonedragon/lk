<?php

namespace App\Factories\Interfaces\Models\Bids;

use App\Http\Requests\Bids\Store\AbstractStoreRequest;
use App\Models\Bids\AbstractApplicant;

interface ApplicantsFactoryInterface
{
    /**
     * @param AbstractStoreRequest $request
     * @return AbstractApplicant
     */
    public function createByRequest(AbstractStoreRequest $request): AbstractApplicant;
}
