<?php

namespace App\Factories\Interfaces\Support\Utils;

use App\Models\User;
use Laravel\Sanctum\NewAccessToken;

interface AccessTokensFactoryInterface
{
    /**
     * @param User $user
     * @return NewAccessToken
     */
    public function createByUser(User $user): NewAccessToken;
}
