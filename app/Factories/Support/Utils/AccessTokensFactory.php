<?php

namespace App\Factories\Support\Utils;

use App\Factories\Interfaces\Support\Utils\AccessTokensFactoryInterface;
use App\Models\User;
use Laravel\Sanctum\NewAccessToken;

class AccessTokensFactory implements AccessTokensFactoryInterface
{
    /**
     * @param User $user
     * @return NewAccessToken
     */
    public function createByUser(User $user): NewAccessToken
    {
        return $user->createToken('default', config('tokens.access.default.abilities'));
    }
}
