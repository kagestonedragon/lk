<?php

namespace App\Factories\Support\Models\Traits;

use App\Support\Media\Interfaces\HasMedia;
use Illuminate\Http\UploadedFile;

trait Mediable
{
    /**
     * @param HasMedia $model
     * @param UploadedFile[]|null $files
     * @param string $collection
     * @return HasMedia
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     */
    public function addMedia(
        HasMedia $model,
        ?array $files,
        string $collection = 'default'
    ): HasMedia {
        if (empty($files)) {
            return $model;
        }

        foreach ($files as $file) {
            $model
                ->addHashedMedia($file)
                ->toMediaCollection($collection);
        }

        return $model;
    }
}
