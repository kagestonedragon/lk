<?php

namespace App\Factories\Support\Models\Traits;

use App\Exceptions\Factories\SaveException;
use Illuminate\Database\Eloquent\Model;

trait Saveable
{
    /**
     * @param Model $model
     * @return Model
     */
    protected function save(Model $model): Model
    {
        if ($model->save() === false) {
            throw new SaveException($model);
        }

        return $model;
    }
}
