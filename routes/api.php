<?php

use App\Http\Controllers\BidsController;
use App\Http\Controllers\BidsStatusesController;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function() {
    /**
     * POST     /api/v1/users
     * POST     /api/v1/users/token
     * GET      /api/v1/users
     */
    Route::prefix('users')
        ->group(function() {
            Route::post('', [UsersController::class, 'store']);
            Route::post('/token', [UsersController::class, 'authenticate']);
            Route::middleware('auth:sanctum')->get('/', function (Request $request) {
                return $request->user();
            });
        });

    /**
     * POST     /api/v1/bids
     * GET      /api/v1/bids
     * GET      /api/v1/bids/{id}
     * GET      /api/v1/bids/statuses
     *
     * POST     /api/v1/bids/corporate
     * GET      /api/v1/bids/corporate
     * GET      /api/v1/bids/corporate/{id}
     * GET      /api/v1/bids/corporate/statuses
     */
    Route::prefix('bids')
        ->middleware('auth:sanctum')
        ->group(function () {
            Route::post('', [BidsController::class, 'store']);
            Route::get('', [BidsController::class, 'index']);
            Route::get('/statuses', [BidsStatusesController::class, 'index']);
            Route::get('/{bid}', [BidsController::class, 'show']);

            Route::prefix('corporate')
                ->name('corporate')
                ->group(function() {
                    Route::post('', [BidsController::class, 'store']);
                    Route::get('', [BidsController::class, 'index']);
                    Route::get('/statuses', [BidsStatusesController::class, 'index']);
                    Route::get('/{bid}', [BidsController::class, 'show']);
                });
        });

});
