<?php

return [
    'access' => [
        'default' => [
            'abilities' => [
                \App\Http\Requests\Bids\Store\Personal\StoreRequest::getAbilityName(),
                \App\Http\Requests\Bids\Store\Corporate\StoreRequest::getAbilityName(),
            ],
        ],
    ],
];
