<?php

return [
    'statuses' => [
        'initial' => 'N',
    ],
    'personal' => [
        'documents' => [
            'general' => [
                'path' => '/bids/%d/personal/%d/',
            ],
            'layout' => [
                'name' => 'Заявка_%s_Документ_расположения_%s.%s',
                'path' => '/bids/%d/personal/%d/layout_documents',
            ],
            'ownership' => [
                'name' => 'Заявка_%s_Документ_право_собственности_%s.%s',
                'path' => '/bids/%s/personal/%d/ownership_documents',
            ],
        ],
    ],
];
